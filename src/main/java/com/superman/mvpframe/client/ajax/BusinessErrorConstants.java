package com.superman.mvpframe.client.ajax;

/**
 * 业务错误码定义
 * 
 * @author superman
 * @version 2018年8月26日上午10:19:27
 */
public class BusinessErrorConstants {

	/** 内部错误 */
	public static final String INTERNAL_ERROR = "400011";
	/** 缺少必要参数 */
	public static final String MISSING_PARAMS = "400001";

}
