package com.superman.mvpframe.client.ajax;

import java.util.Map;

/**
 * Ajax post请求发送接口定义
 * 
 * @author superman
 * @version 2018年7月12日下午2:20:53
 */
public interface IPostMan {
	/**
	 * 发送post请求
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午2:21:12
	 * @param path 请求的路径
	 * @param postData 请求体
	 * @param callback 异步回调接口
	 */
	void post(String path, Map<String, Object> postData, IPostManAsyncCallBack callback);

	/**
	 * 发送post请求,同时带上spinner等待的提示
	 * 
	 * @author superman
	 * @version 2018年8月15日 下午3:04:42
	 * @param path
	 * @param postData
	 * @param callback
	 */
	void postWithLoadingSpinner(String path, Map<String, Object> postData, IPostManAsyncCallBack callback);
}
