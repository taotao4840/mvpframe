package com.superman.mvpframe.client.ajax;

/**
 * Ajax 异步回调接口
 * 
 * @author superman
 * @version 2018年7月12日下午2:22:03
 */
public interface IPostManAsyncCallBack {
	/**
	 * http异常
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午2:22:15
	 * @param fullPostUrl 请求路径
	 * @param statusCode http状态码
	 */
	void onHttpError(String fullPostUrl, int statusCode);

	/**
	 * http正常
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午2:22:17
	 * @param fullPostUrl 请求路径
	 * @param response 响应保文
	 */
	void onHttpOK(String fullPostUrl, String response);
}
