package com.superman.mvpframe.client.ajax;

import java.util.Map;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.superman.mvpframe.client.gin.XGinjector;
import com.superman.mvpframe.client.utils.LogUtils;
import com.superman.mvpframe.client.utils.StringUtils;
import com.superman.mvpframe.client.utils.XJsonUtil;
import com.superman.mvpframe.client.widget.spinner.TimerSpinnerHelper;

/**
 * Ajax post请求发送接口默认实现类
 * 
 * @author superman
 * @version 2018年7月12日下午2:23:21
 */
public class PostManDefaultImpl implements IPostMan {

	private final String CUSTOM_HEADER_KEY = "SESSIONID";
	private final long DELAY_MILL_TIME = 3 * 1000;

	/**
	 * JSNI方法拼接完整的请求路径
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午3:24:25
	 * @param path
	 * @return
	 */
	private native String getFullPostPath0(String path)/*-{
														if (path.indexOf('/') == 0)
														path = path.substring(1);
														return $wnd.api + "/" + path;
														}-*/;

	private String getSessionId() {
		return XGinjector.INSTANCE.getUserModel().getSessionid();
	}

	private void post(final boolean withLoadingSpinner, String path, Map<String, Object> postData, final IPostManAsyncCallBack callback) {

		final String fullPath = getFullPostPath0(path);
		final RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, fullPath);
		requestBuilder.setHeader("content-type", "application/json;charset=utf-8");

		if (!StringUtils.isEmpty(getSessionId()))
			requestBuilder.setHeader(CUSTOM_HEADER_KEY, getSessionId());

		String postJsonStr = XJsonUtil.objMapToJsonObj(postData).toString();
		String signStr = sign0(postJsonStr, getSessionId());
		requestBuilder.setRequestData(signStr);

		LogUtils.logPost(fullPath, getSessionId(), postJsonStr);

		requestBuilder.setCallback(new RequestCallback() {

			@Override
			public void onError(Request request, Throwable exception) {
				callback.onHttpError(fullPath, -1);
				if (withLoadingSpinner)
					TimerSpinnerHelper.getInstance().hide();
			}

			@Override
			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == Response.SC_OK) {
					callback.onHttpOK(fullPath, response.getText());
				} else {
					callback.onHttpError(fullPath, response.getStatusCode());
				}
				if (withLoadingSpinner)
					TimerSpinnerHelper.getInstance().hide();

			}
		});

		try {
			if (withLoadingSpinner)
				TimerSpinnerHelper.getInstance().show();
			requestBuilder.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void post(String path, Map<String, Object> postData, IPostManAsyncCallBack callback) {
		post(false, path, postData, callback);
	}

	@Override
	public void postWithLoadingSpinner(String path, Map<String, Object> postData, IPostManAsyncCallBack callback) {
		post(true, path, postData, callback);
	}

	/**
	 * JSNI调用签名方法
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午3:24:04
	 * @param postJsonStr
	 * @param sessionID
	 * @return
	 */
	private native String sign0(String postJsonStr, String sessionID)/*-{
																		return $wnd.signinopen(postJsonStr, sessionID);
																		}-*/;

}
