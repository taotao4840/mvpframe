package com.superman.mvpframe.client.dom;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

/**
 * 具备绑定input监听处理器的接口
 * 
 * @author superman
 * @version 2018年8月14日上午10:27:50
 */
public interface HasInputHandlers extends HasHandlers {
	/**
	 * 添加input事件的监听
	 * 
	 * @author superman
	 * @version 2018年8月14日 上午10:27:52
	 * @param handler
	 * @return HandlerRegistration
	 */
	HandlerRegistration addInputHandler(InputHandler handler);
}
