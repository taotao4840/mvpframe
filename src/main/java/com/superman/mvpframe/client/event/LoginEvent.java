package com.superman.mvpframe.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.superman.mvpframe.client.event.LoginEvent.LoginEventHandler;

/**
 * 登录事件
 * 
 * @author superman
 * @version 2018年7月12日下午4:21:31
 */
public class LoginEvent extends GwtEvent<LoginEventHandler> {

	/**
	 * 事件处理器
	 * 
	 * @author superman
	 * @version 2018年7月12日下午4:22:00
	 */
	public interface LoginEventHandler extends EventHandler {
		/**
		 * 登录
		 * 
		 * @author superman
		 * @version 2018年7月12日 下午4:22:02
		 * @param event
		 */
		void login(LoginEvent event);
	}

	public static final Type<LoginEventHandler> TYPE = new Type<LoginEventHandler>();

	@Override
	protected void dispatch(LoginEventHandler handler) {
		handler.login(this);
	}

	@Override
	public Type<LoginEventHandler> getAssociatedType() {
		return TYPE;
	}

}
