package com.superman.mvpframe.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.superman.mvpframe.client.event.ReloadEvent.ReloadEventHandler;

/**
 * 重载事件
 * 
 * @author superman
 * @version 2018年7月12日下午4:21:31
 */
public class ReloadEvent extends GwtEvent<ReloadEventHandler> {

	/**
	 * 事件处理器
	 * 
	 * @author superman
	 * @version 2018年7月12日下午4:22:00
	 */
	public interface ReloadEventHandler extends EventHandler {
		/**
		 * 重载
		 * 
		 * @author superman
		 * @version 2018年7月12日 下午4:22:02
		 * @param event
		 */
		void reload(ReloadEvent event);
	}

	public static final Type<ReloadEventHandler> TYPE = new Type<ReloadEventHandler>();

	@Override
	protected void dispatch(ReloadEventHandler handler) {
		handler.reload(this);
	}

	@Override
	public Type<ReloadEventHandler> getAssociatedType() {
		return TYPE;
	}

}
