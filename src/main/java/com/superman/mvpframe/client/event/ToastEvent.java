package com.superman.mvpframe.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.superman.mvpframe.client.event.ToastEvent.ToastEventHandler;

/**
 * toast事件.需要toast提示的都可以通过eventbus触发该事件
 * 
 * @author superman
 * @version 2018年7月12日下午4:21:31
 */
public class ToastEvent extends GwtEvent<ToastEventHandler> {

	/**
	 * 事件处理器
	 * 
	 * @author superman
	 * @version 2018年7月12日下午4:22:00
	 */
	public interface ToastEventHandler extends EventHandler {
		/**
		 * toast
		 * 
		 * @author superman
		 * @version 2018年7月12日 下午4:22:02
		 * @param event
		 */
		void toast(ToastEvent event);
	}

	public static final Type<ToastEventHandler> TYPE = new Type<ToastEventHandler>();

	private String message;

	public ToastEvent(String message) {
		super();
		this.message = message;
	}

	@Override
	protected void dispatch(ToastEventHandler handler) {
		handler.toast(this);
	}

	@Override
	public Type<ToastEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getMessage() {
		return message;
	}

}
