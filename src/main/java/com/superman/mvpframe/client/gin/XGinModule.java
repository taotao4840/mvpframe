package com.superman.mvpframe.client.gin;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.superman.mvpframe.client.ajax.IPostMan;
import com.superman.mvpframe.client.history.IHtml5Historian;
import com.superman.mvpframe.client.model.UserModel;
import com.superman.mvpframe.client.mvp.MyPlaceController;
import com.superman.mvpframe.client.router.IPageRouter;
import com.superman.mvpframe.client.router.MyPageRouterPopHelper;
import com.superman.mvpframe.client.utils.ActivityEventbusController;

/**
 * 项目gin模块绑定类
 * 
 * @author superman
 * @version 2018年7月5日下午9:39:31
 */
public class XGinModule extends AbstractGinModule {

	@Override
	protected void configure() {
		bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
		bind(IPageRouter.class).in(Singleton.class);
		bind(IPostMan.class).in(Singleton.class);
		bind(IHtml5Historian.class).in(Singleton.class);
		bind(MyPlaceController.class).in(Singleton.class);
		bind(MyPageRouterPopHelper.class).in(Singleton.class);
		bind(UserModel.class).in(Singleton.class);
		bind(ActivityEventbusController.class).in(Singleton.class);

	}

}
