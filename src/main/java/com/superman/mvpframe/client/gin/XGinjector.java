package com.superman.mvpframe.client.gin;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.superman.mvpframe.client.ajax.IPostMan;
import com.superman.mvpframe.client.history.IHtml5Historian;
import com.superman.mvpframe.client.model.UserModel;
import com.superman.mvpframe.client.mvp.MyPlaceController;
import com.superman.mvpframe.client.router.IPageRouter;
import com.superman.mvpframe.client.router.MyPageRouterPopHelper;
import com.superman.mvpframe.client.utils.ActivityEventbusController;

/**
 * 项目gin模块
 * 
 * @author superman
 * @version 2018年7月5日下午9:37:45
 */
@GinModules(XGinModule.class)
public interface XGinjector extends Ginjector {

	// 通过gwt.create构建实例
	public static final XGinjector INSTANCE = GWT.create(XGinjector.class);

	EventBus getEventBus();

	ActivityEventbusController getEventbusController();

	IHtml5Historian getHtml5Historian();

	MyPlaceController getMyPlaceController();

	IPageRouter getPageRouter();

	MyPageRouterPopHelper getPageRouterPopHelper();

	IPostMan getPostMan();

	UserModel getUserModel();

}
