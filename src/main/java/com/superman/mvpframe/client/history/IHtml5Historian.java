package com.superman.mvpframe.client.history;

import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * 浏览器历史栈接口
 * 
 * @author superman
 * @version 2018年7月5日下午10:24:52
 */
public interface IHtml5Historian {
	/**
	 * 添加pop事件的监听
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:55
	 * @param handler
	 * @return HandlerRegistration
	 */
	public HandlerRegistration addPopStateHandler(PopStateHandler handler);

	/**
	 * 后退
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:42
	 */
	public void back();

	/**
	 * 前进
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:40
	 */
	public void forward();

	/**
	 * 前进指定几个页面
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:45
	 * @param number
	 */
	public void go(int number);

	/**
	 * 获取当前历史栈的长度
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:47
	 * @return 长度
	 */
	public int length();

	/**
	 * 压入新的历史栈
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:50
	 * @param data
	 * @param title
	 * @param url
	 */
	public void pushState(String data, String title, String url);

	/**
	 * 替换当前顶层的历史栈
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:25:52
	 * @param data
	 * @param title
	 * @param url
	 */
	public void replaceState(String data, String title, String url);

}
