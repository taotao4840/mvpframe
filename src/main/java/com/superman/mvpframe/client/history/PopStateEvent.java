package com.superman.mvpframe.client.history;

import com.google.web.bindery.event.shared.Event;

/**
 * 支持h5 pop特性的事件定义
 * 
 * @author superman
 * @version 2018年7月5日下午10:23:46
 */
public class PopStateEvent extends Event<PopStateHandler> {

	private static final Type<PopStateHandler> TYPE = new Type<PopStateHandler>();

	public static Type<PopStateHandler> getType() {
		return TYPE;
	}

	private final String data;
	private final String title;

	private final String url;

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:24:21
	 * @param data
	 * @param title
	 * @param url
	 */
	public PopStateEvent(String data, String title, String url) {
		this.data = data;
		this.title = title;
		this.url = url;
	}

	@Override
	protected void dispatch(PopStateHandler handler) {
		handler.onPopStateEvent(this);

	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<PopStateHandler> getAssociatedType() {
		return TYPE;
	}

	public String getData() {
		return data;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

}
