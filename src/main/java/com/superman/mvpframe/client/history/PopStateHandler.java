package com.superman.mvpframe.client.history;

import com.google.gwt.event.shared.EventHandler;

/**
 * pop事件处理器
 * 
 * @author superman
 * @version 2018年7月5日下午10:22:51
 */
public interface PopStateHandler extends EventHandler {
	/**
	 * 事件出发
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:24:36
	 * @param event
	 */
	public void onPopStateEvent(PopStateEvent event);
}