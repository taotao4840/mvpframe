package com.superman.mvpframe.client.model;

import com.google.gwt.json.client.JSONObject;

/**
 * 原型接口.这里定义了原型的方法
 * 
 * @author superman
 * @version 2018年7月12日下午5:23:59
 */
public interface IShopModel {

	/**
	 * 清除所有的数据
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午5:25:07
	 */
	void clear();

	/**
	 * 解析json
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午5:25:13
	 * @param jo
	 */
	void parse(JSONObject jo);

	/**
	 * 从缓存读取数据
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午5:25:11
	 */
	void readFromCache();

	/**
	 * 更新到缓存
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午5:25:09
	 */
	void saveToCache();

}
