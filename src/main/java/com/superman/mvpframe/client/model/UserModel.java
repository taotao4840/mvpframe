package com.superman.mvpframe.client.model;

import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.storage.client.Storage;
import com.superman.mvpframe.client.utils.StringUtils;

/**
 * 用户原型.落地到localstorage中
 * 
 * @author superman
 * @version 2018年7月12日下午5:26:09
 */
public class UserModel implements IShopModel {

	private static final String CACHE_KEY = "SILENT_LOGIN_USER";

	private String addr = "";// 地址
	private String birthday = "";// 生日
	private String icon = "";// 用户头像
	private String nickname = "";// 用户昵称
	private String sessionid = "";// 用户的sessionid
	private int sex = 0;// 用户性别
	private String tel = "";// 用户手机号
	private String userid = "";// 用户的userid

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年8月2日 下午6:02:33
	 */
	public UserModel() {
		super();
	}

	@Override
	public void clear() {
		this.sessionid = "";
		this.userid = "";
		this.tel = "";
		this.nickname = "";
		this.sex = 1;
		this.icon = "";
		this.addr = "";
		this.birthday = "";

		Storage.getLocalStorageIfSupported().removeItem(CACHE_KEY);
	}

	public String getIcon() {
		return StringUtils.isEmpty(icon) ? "images/my/header_default.png" : icon;
	}

	public String getNickname() {
		return nickname;
	}

	public String getSessionid() {
		// 调试模式的时候 ,返回一个固定的sessionid
		return GWT.isProdMode() ? sessionid : "VXKRZTYSTEFJGIEMWHALHAWFGBZLSXLK";
	}

	public int getSex() {
		return sex;
	}

	public String getTel() {
		return tel;
	}

	public String getUserid() {
		return userid;
	}

	@Override
	public void parse(JSONObject jo) {
		if (jo.get("SESSION_ID") != null)
			this.sessionid = jo.get("SESSION_ID").isString().stringValue();
		if (jo.get("USER_ID") != null)
			this.userid = jo.get("USER_ID").isString().stringValue();
		if (jo.get("PHONE") != null)
			this.tel = jo.get("PHONE").isString().stringValue();
		if (jo.get("NICK_NAME") != null)
			this.nickname = jo.get("NICK_NAME").isString().stringValue();
		if (jo.get("SEX") != null)
			this.sex = (int) jo.get("SEX").isNumber().doubleValue();
		if (jo.get("ICON") != null)
			this.icon = jo.get("ICON").isString().stringValue();
		if (jo.get("ADDR") != null)
			this.addr = jo.get("ADDR").isString().stringValue();
		if (jo.get("BIRTHDAY") != null)
			this.birthday = jo.get("BIRTHDAY").isString().stringValue();
	}

	@Override
	public void readFromCache() {
		String cache = Storage.getLocalStorageIfSupported().getItem(CACHE_KEY);
		if (!StringUtils.isEmpty(cache)) {
			JSONObject jsonObject = JSONParser.parseStrict(cache).isObject();
			parse(jsonObject);
		}
	}

	@Override
	public void saveToCache() {
		JSONObject jsonObject = new JSONObject();
		if (!StringUtils.isEmpty(this.sessionid))
			jsonObject.put("SESSION_ID", new JSONString(this.sessionid));

		if (!StringUtils.isEmpty(this.userid))
			jsonObject.put("USER_ID", new JSONString(this.userid));

		if (!StringUtils.isEmpty(this.tel))
			jsonObject.put("PHONE", new JSONString(this.tel));

		if (!StringUtils.isEmpty(this.nickname))
			jsonObject.put("NICK_NAME", new JSONString(this.nickname));

		jsonObject.put("SEX", new JSONNumber(this.sex));

		if (!StringUtils.isEmpty(this.icon))
			jsonObject.put("ICON", new JSONString(this.icon));

		if (!StringUtils.isEmpty(this.addr))
			jsonObject.put("ADDR", new JSONString(this.addr));

		if (!StringUtils.isEmpty(this.birthday))
			jsonObject.put("BIRTHDAY", new JSONString(this.birthday));

		Storage.getLocalStorageIfSupported().setItem(CACHE_KEY, jsonObject.toString());

	}

}
