package com.superman.mvpframe.client.mvp;

import java.util.LinkedList;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.superman.mvpframe.client.ajax.IPostMan;
import com.superman.mvpframe.client.gin.XGinjector;

/**
 * activity基础类
 * 
 * @author superman
 * @version 2018年7月5日下午10:35:18
 */
public abstract class AbstractMVPActivity extends AbstractActivity implements IActivityLifeCircle {

	protected EventBus eventBus = XGinjector.INSTANCE.getEventBus();
	private LinkedList<com.google.web.bindery.event.shared.HandlerRegistration> handlers;

	private LinkedList<HandlerRegistration> oldHandlers;
	protected MyPlaceController placeController = XGinjector.INSTANCE.getMyPlaceController();
	protected IPostMan postman = XGinjector.INSTANCE.getPostMan();

	private int top;

	private boolean firstInit = true;

	@Override
	public boolean isFirstInit() {
		return firstInit;
	}

	@Override
	public void setFirstInit(boolean firstInit) {
		this.firstInit = firstInit;
	}

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:41:33
	 */
	public AbstractMVPActivity() {
		oldHandlers = new LinkedList<HandlerRegistration>();
		handlers = new LinkedList<com.google.web.bindery.event.shared.HandlerRegistration>();
	}

	/**
	 * add a {@link HandlerRegistration} to the handler collection
	 *
	 * @param handlerRegistration a
	 *        {@link com.google.gwt.event.shared.HandlerRegistration} object.
	 */
	protected void addHandlerRegistration(com.google.web.bindery.event.shared.HandlerRegistration handlerRegistration) {
		handlers.add(handlerRegistration);
	}

	/**
	 * add a {@link HandlerRegistration} to the handler collection
	 *
	 * @param handlerRegistration a
	 *        {@link com.google.gwt.event.shared.HandlerRegistration} object.
	 */
	protected void addHandlerRegistration(HandlerRegistration handlerRegistration) {
		oldHandlers.add(handlerRegistration);
	}

	/**
	 * Remove all collected oldHandlers, and remove them from the collection
	 */
	protected void cancelAllHandlerRegistrations() {
		for (HandlerRegistration hr : oldHandlers) {
			hr.removeHandler();
		}
		oldHandlers.clear();

		for (com.google.web.bindery.event.shared.HandlerRegistration hr : handlers) {
			hr.removeHandler();
		}
		handlers.clear();
	}

	public int getTop() {
		return top;
	}

	/**
	 * 获取页面View接口
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:46:22
	 * @return AppView
	 */
	public abstract IPageView getView();

	/**
	 * 获取页面的P层接口
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:46:24
	 * @return IPageViewPresenter
	 */
	public abstract IPageViewPresenter getViewPresenter();

	/**
	 * 页面激活.此方法不区分页面的前进还是后退
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:46:20
	 * @param eventBus
	 */
	public abstract void onActivity(EventBus eventBus);

	/**
	 * {@inheritDoc} onStop is overriden to automatically clear all
	 * {@link HandlerRegistration}
	 */
	@Override
	public void onStop() {
		super.onStop();

		cancelAllHandlerRegistrations();
	}

	public void setTop(int top) {
		this.top = top;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		XGinjector.INSTANCE.getEventbusController().setEventBus(eventBus);
		IPageView appView = getView();
		panel.setWidget(appView);
		appView.init();

		IPageViewPresenter presenter = getViewPresenter();
		if (presenter != null)
			appView.setPageViewPresenter(presenter);

		onActivity(eventBus);

	}

}
