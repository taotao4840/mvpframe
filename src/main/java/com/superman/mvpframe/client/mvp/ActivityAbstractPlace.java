package com.superman.mvpframe.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.place.shared.Place;

/**
 * 抽象的路由类
 * 
 * @author superman
 * @version 2018年7月5日下午10:53:55
 * @param <T>
 */
public abstract class ActivityAbstractPlace<T extends Activity> extends Place {

	protected T activity;

	/**
	 * 获取当前已经初始化的activity
	 * 
	 * @author superman
	 * @version 2018年8月8日 下午2:50:15
	 * @return activity
	 */
	public T getActivity() {
		return activity;
	}

	/**
	 * 初始化activity.通过AsyncProvider异步初始化,实现代码分割
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午5:16:34
	 * @param activityAsyncCallBackHandler
	 */
	public abstract void getActivity(ActivityAsyncCallBackHandler activityAsyncCallBackHandler);

	protected void onActivityCreated(T activity) {
		this.activity = activity;
	}

}
