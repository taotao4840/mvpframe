package com.superman.mvpframe.client.mvp;

import com.google.gwt.activity.shared.Activity;

/**
 * activity初始化异步回调接口.此接口是实现代码分割的关键接口
 * 
 * @author superman
 * @version 2018年7月5日下午10:55:07
 */
public interface ActivityAsyncCallBackHandler {
	/**
	 * 初始化成功
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:56:24
	 * @param instance
	 */
	public void onReceive(Activity instance);
}
