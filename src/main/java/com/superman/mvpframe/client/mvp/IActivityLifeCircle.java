package com.superman.mvpframe.client.mvp;

/**
 * 页面的生命周期接口定义
 * 
 * @author superman
 * @version 2018年7月5日下午10:40:00
 */
public interface IActivityLifeCircle {

	/**
	 * 页面返回
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:40:39
	 */
	void popToRefresh();

	/**
	 * 页面返回.带上下文
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:40:47
	 * @param contextData
	 */
	void popToRefresh(Object contextData);

	/**
	 * 页面前进
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:39:37
	 */
	void pushToRefresh();

	/**
	 * 是否首次加载
	 * 
	 * @author superman
	 * @version 2018年10月18日 下午9:22:46
	 * @return true or false
	 */
	boolean isFirstInit();

	/**设置是否首次加载
	 * 
	 * 
	 * @author superman
	 * @version 2018年10月18日 下午9:22:49
	 * @param firstInit
	 */
	void setFirstInit(boolean firstInit);

}
