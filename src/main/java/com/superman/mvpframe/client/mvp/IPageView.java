package com.superman.mvpframe.client.mvp;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * MVP中的V层接口
 * 
 * @author superman
 * @version 2018年7月5日下午10:49:02
 */
public interface IPageView extends IsWidget {

	/**
	 * 初始化ui.一般这里不做数据的解析交互处理,制作一些ui的初始化和数据的初始化操作
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:49:40
	 */
	public void init();

	/**
	 * 设置P层接口
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午10:49:19
	 * @param presenter
	 */
	public void setPageViewPresenter(IPageViewPresenter presenter);

}
