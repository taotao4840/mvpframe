package com.superman.mvpframe.client.mvp;

import java.util.Map;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.place.shared.Place;
import com.superman.mvpframe.client.mvp.MyPlaceChangeEvent.MyPlaceChangeEventHandler;

/**
 * 路由发生变化后触发的事件
 * 
 * @author superman
 * @version 2018年7月11日下午1:54:45
 */
public class MyPlaceChangeEvent extends GwtEvent<MyPlaceChangeEventHandler> {

	/**
	 * 路由发生变化后触发的事件监听器
	 * 
	 * @author superman
	 * @version 2018年7月11日下午1:55:07
	 */
	public interface MyPlaceChangeEventHandler extends EventHandler {
		/**
		 * 路由发生变化
		 * 
		 * @author superman
		 * @version 2018年7月11日 下午1:55:09
		 * @param event
		 */
		void onPlaceChange(MyPlaceChangeEvent event);
	}

	public static final Type<MyPlaceChangeEventHandler> TYPE = new Type<MyPlaceChangeEventHandler>();

	private Map<String, Object> contextData;

	private boolean isBack;

	private Place place;

	@Override
	protected void dispatch(MyPlaceChangeEventHandler handler) {
		handler.onPlaceChange(this);
	}

	@Override
	public Type<MyPlaceChangeEventHandler> getAssociatedType() {
		return TYPE;
	}

	public Map<String, Object> getContextData() {
		return contextData;
	}

	public Place getPlace() {
		return place;
	}

	public boolean isBack() {
		return isBack;
	}

	public void setBack(boolean isBack) {
		this.isBack = isBack;
	}

	public void setContextData(Map<String, Object> contextData) {
		this.contextData = contextData;
	}

	public void setPlace(Place place) {
		this.place = place;
	}
}
