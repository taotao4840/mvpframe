package com.superman.mvpframe.client.mvp;

import com.superman.mvpframe.client.utils.Registry;

/**
 * urlhash及place的关联类
 * 
 * @author superman
 * @version 2018年8月8日上午11:03:27
 */
public class URLHashAndPlaceMapper extends Registry {

	/**
	 * 是否为空
	 * 
	 * @author superman
	 * @version 2018年8月21日 下午11:40:21
	 * @return
	 */
	public static boolean isEmpty() {
		return map.size() == 0;
	}

}
