package com.superman.mvpframe.client.router;

import java.util.Map;

import com.google.gwt.place.shared.Place;

/**
 * 路由接口定义
 * 
 * @author superman
 * @version 2018年7月5日下午9:46:58
 */
public interface IPageRouter {

	/**
	 * 返回到指定的页面,并且可以携带上下文参数,参数将会以url参数的方式携带
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午9:47:13
	 * @param place 返回的页面路由
	 * @param contextData 上下文参数
	 */
	void back(Place place, Map<String, Object> contextData);

	/**
	 * 返回到首页
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午9:47:22
	 */
	void backHome();

	/**
	 * 页面前进,并且可以携带上下文参数.参数将会以url参数的方式携带
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午9:47:11
	 * @param place 前进的页面路由
	 * @param contextData 上下文参数
	 */
	void push(Place place, Map<String, Object> contextData);

}
