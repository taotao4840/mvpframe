package com.superman.mvpframe.client.router;

import java.util.Map;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.place.shared.Place;
import com.superman.mvpframe.client.router.PagePopEvent.PagePopEventHandler;

/**
 * 页面返回事件定义
 * 
 * @author superman
 * @version 2018年7月5日下午9:53:16
 */
public class PagePopEvent extends GwtEvent<PagePopEventHandler> {

	/**
	 * 页面路由事件处理器
	 * 
	 * @author superman
	 * @version 2018年7月5日下午9:54:44
	 */
	public interface PagePopEventHandler extends EventHandler {
		/**
		 * 返回
		 * 
		 * @author superman
		 * @version 2018年7月5日 下午9:54:46
		 * @param event 返回事件
		 */
		void onPop(PagePopEvent event);

	}

	public static final Type<PagePopEventHandler> TYPE = new Type<PagePopEventHandler>();

	private boolean backHome = false;

	private Map<String, Object> contextData;

	private Place place;

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月5日 下午9:55:26
	 */
	public PagePopEvent() {
		super();
	}

	@Override
	protected void dispatch(PagePopEventHandler handler) {
		handler.onPop(this);
	}

	@Override
	public Type<PagePopEventHandler> getAssociatedType() {
		return TYPE;
	}

	public Map<String, Object> getContextData() {
		return contextData;
	}

	public Place getPlace() {
		return place;
	}

	public boolean isBackHome() {
		return backHome;
	}

	public void setBackHome(boolean backHome) {
		this.backHome = backHome;
	}

	public void setContextData(Map<String, Object> contextData) {
		this.contextData = contextData;
	}

	public void setPlace(Place place) {
		this.place = place;
	}
}
