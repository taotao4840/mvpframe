package com.superman.mvpframe.client.router;

import java.util.Map;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.superman.mvpframe.client.event.LoginEvent;
import com.superman.mvpframe.client.gin.XGinjector;
import com.superman.mvpframe.client.mvp.MyPlaceChangeEvent;
import com.superman.mvpframe.client.utils.StringUtils;

/**
 * 基于mvp架构的路由实现
 * 
 * @author superman
 * @version 2018年7月5日下午9:58:39
 */
public class PageRouterMVPImpl implements IPageRouter {

	private final EventBus eventBus = XGinjector.INSTANCE.getEventBus();

	@Override
	public void back(Place place, Map<String, Object> contextData) {
		PagePopEvent pageRouterEvent = new PagePopEvent();
		if (place != null)
			pageRouterEvent.setPlace(place);
		if (contextData != null)
			pageRouterEvent.setContextData(contextData);

		eventBus.fireEvent(pageRouterEvent);

	}

	@Override
	public void backHome() {
		PagePopEvent pageRouterEvent = new PagePopEvent();
		pageRouterEvent.setBackHome(true);
		eventBus.fireEvent(pageRouterEvent);

	}

	@Override
	public void push(Place place, Map<String, Object> contextData) {
		boolean readyToPush = true;
		if (place instanceof INeedLoginPlace) {
			if (StringUtils.isEmpty(XGinjector.INSTANCE.getUserModel().getSessionid())) {
				// 当前没有登录.发起登录事件
				eventBus.fireEvent(new LoginEvent());
				readyToPush = false;
			}
		}
		if (readyToPush) {
			MyPlaceChangeEvent myPlaceChangeEvent = new MyPlaceChangeEvent();
			myPlaceChangeEvent.setPlace(place);
			if (contextData != null)
				myPlaceChangeEvent.setContextData(contextData);
			eventBus.fireEvent(myPlaceChangeEvent);
		}
	}
}
