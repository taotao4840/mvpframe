package com.superman.mvpframe.client.utils;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 * 单个activity生命周期内的eventbus
 * 
 * @author superman
 * @version 2018年8月9日下午2:30:30
 */
public class ActivityEventbusController {
	private EventBus eventBus;

	/**
	 * 监听
	 * 
	 * @author superman
	 * @version 2018年8月9日 下午2:30:39
	 * @param type
	 * @param handler
	 * @return HandlerRegistration
	 */
	public <H extends EventHandler> HandlerRegistration addHandler(GwtEvent.Type<H> type, H handler) {
		return eventBus.addHandler(type, handler);
	}

	/**
	 * 触发
	 * 
	 * @author superman
	 * @version 2018年8月9日 下午2:30:36
	 * @param event
	 */
	public <H extends EventHandler> void fire(GwtEvent<H> event) {
		eventBus.fireEvent(event);
	}

	/**
	 * 设置eventbus
	 * 
	 * @author superman
	 * @version 2018年8月9日 下午2:30:33
	 * @param eventBus
	 */
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}

}
