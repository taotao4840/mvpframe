package com.superman.mvpframe.client.utils;

/**
 * dom工具类
 * 
 * @author superman
 * @version 2018年8月22日上午9:55:57
 */
public class DomUtils {

	/**
	 * 文档的总高度
	 * 
	 * @author superman
	 * @version 2018年8月22日 上午9:55:59
	 * @return scrollheight
	 */
	public static double getScrollHeight() {
		return getScrollHeight0();
	}

	/**
	 * 绑定优化移动端点击
	 * 
	 * @author superman
	 * @version 2018年8月22日 下午10:49:57
	 */
	public native static void bindFastClick()/*-{
		$wnd.FastClick.attach($wnd.document.body);
	}-*/;

	/**
	 * 文档的总高度
	 * 
	 * @author superman
	 * @version 2017年6月6日 下午2:58:30
	 * @return
	 */
	private static native double getScrollHeight0()/*-{
													var scrollHeight = 0,
													bodyScrollHeight = 0,
													documentScrollHeight = 0;　　
													if($doc.body) {　　　　
													bodyScrollHeight = $doc.body.scrollHeight;　　
													}　　
													if($doc.documentElement) {　　　　
													documentScrollHeight = $doc.documentElement.scrollHeight;　　
													}　　
													scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;　　
													return scrollHeight;
													}-*/;

	/**
	 * 滚动条在Y轴上的滚动距离
	 * 
	 * @author superman
	 * @version 2018年8月22日 上午9:56:01
	 * @return scrolltop
	 */
	public static double getScrollTop() {
		return getScrollTop0();
	}

	/**
	 * 滚动条在Y轴上的滚动距离
	 * 
	 * @author superman
	 * @version 2017年6月6日 下午2:58:11
	 * @return
	 */
	private static native double getScrollTop0()/*-{
												var scrollTop = 0,
												bodyScrollTop = 0,
												documentScrollTop = 0;　　
												if($doc.body) {　　　　
												bodyScrollTop = $doc.body.scrollTop;　　
												}　　
												if($doc.documentElement) {　　　　
												documentScrollTop = $doc.documentElement.scrollTop;　　
												}　　
												scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;　　
												return scrollTop;
												}-*/;

	/**
	 * 浏览器视口的高度
	 * 
	 * @author superman
	 * @version 2018年8月22日 上午9:56:03
	 * @return windowheight
	 */
	public static double getWindowHeight() {
		return getWindowHeight0();
	}

	/**
	 * 浏览器视口的高度
	 * 
	 * @author superman
	 * @version 2017年6月6日 下午2:58:36
	 * @return
	 */
	private static native double getWindowHeight0()/*-{
													var windowHeight = 0;　　
													if($doc.compatMode == "CSS1Compat") {　　　　
													windowHeight = $doc.documentElement.clientHeight;　　
													} else {　　　　
													windowHeight = $doc.body.clientHeight;　　
													}　　
													return windowHeight;
													}-*/;

	/**
	 * 是否滚动到屏幕底部边缘
	 * 
	 * @author superman
	 * @version 2018年8月22日 上午9:56:06
	 * @return true or false
	 */
	public static boolean isScrollToBottom() {
		return getScrollTop0() + getWindowHeight0() >= getScrollHeight0() - MobileUtils.px2(50);
	}
}
