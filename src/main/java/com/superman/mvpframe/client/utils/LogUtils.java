package com.superman.mvpframe.client.utils;

import com.google.gwt.http.client.Response;

/**
 * 日志工具类
 * 
 * @author superman
 * @version 2018年7月12日下午3:43:16
 */
public class LogUtils {
	private static final int ERROR = 3;
	// 以下是日志输出的级别
	private static final int INFO = 1;
	private static final int WARN = 2;

	private static native void console0(String msg, int level)/*-{
																if (level == 1) {
																$wnd.console.log(msg);
																} else if (level == 2) {
																$wnd.console.warn(msg);
																} else if (level == 3) {
																$wnd.console.error(msg);
																}
																}-*/;

	private static native int getLogLevel0()/*-{
											return $wnd.LOGLEVEL;
											}-*/;

	private static native boolean isOpenLog0()/*-{
												return $wnd.OUTPUT_LOG;
												}-*/;

	private static void logOnConsole(String msg, int level) {
		if (isOpenLog0()) {
			if (level >= getLogLevel0()) {
				console0(msg, level);
			}
		}
	}

	/**
	 * 控制台输出post请求日志.
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午3:56:06
	 * @param fullPostUrl 完整的请求路径
	 * @param sessionID sessionid
	 * @param postData 请求保文
	 */
	public static void logPost(String fullPostUrl, String sessionID, String postData) {
		StringBuffer sb = new StringBuffer();
		sb.append("@@@@@@@@@@");
		sb.append("请求准备发送");
		sb.append(",路径=");
		sb.append(fullPostUrl);
		sb.append(",SESSIONID=");
		sb.append(sessionID);
		sb.append(",请求保文=");
		sb.append(postData);
		sb.append("@@@@@@@@@@");
		sb.append("---------");
		logOnConsole(sb.toString(), INFO);
	}

	/**
	 * 控制台输出post响应日志
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午3:56:09
	 * @param fullPostUrl 完整的请求路径
	 * @param statusCode http状态码
	 * @param receivedData 响应保文
	 */
	public static void logPostReceived(String fullPostUrl, int statusCode, String receivedData) {
		StringBuffer sb = new StringBuffer();
		sb.append("$$$$$$$$$$");
		sb.append("请求响应成功");
		sb.append(",路径=");
		sb.append(fullPostUrl);
		sb.append(",状态码=");
		sb.append(statusCode);
		sb.append(",响应保文=");
		sb.append(receivedData);
		sb.append("$$$$$$$$$$");
		sb.append("---------");
		logOnConsole(sb.toString(), statusCode == Response.SC_OK ? INFO : ERROR);
	}

}
