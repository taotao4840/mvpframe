package com.superman.mvpframe.client.utils;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.Window;

/**
 * 手机端工具类
 * 
 * @author superman
 * @version 2018年8月3日下午4:10:58
 */
public class MobileUtils {

	/**
	 * 将px转换成rem
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:11:01
	 * @param pxValue
	 * @return
	 */
	public static int px2(int pxValue) {
		int w = Window.getClientWidth();
		return pxValue * w / 750;
	}

	/**
	 * 设置页面的背景色
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:11:03
	 * @param color
	 */
	public static void setPageBgColor(String color) {
		Document.get().getElementsByTagName("head").getItem(0).getParentElement().getStyle().setBackgroundColor(color);
	}

}
