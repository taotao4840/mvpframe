package com.superman.mvpframe.client.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 共享类.一个实例生命周期内有效
 * 
 * @author superman
 * @version 2018年8月3日下午4:14:41
 */
public class Registry {
	protected static Map<String, Object> map = new HashMap<String, Object>();

	/**
	 * 根据id取回内存中的数据
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:14:52
	 * @param id
	 * @return
	 */
	public static <X> X get(String id) {
		return (X) map.get(id);
	}

	/**
	 * 置入数据到共享类
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:15:25
	 * @param id
	 * @param obj
	 */
	public static void register(String id, Object obj) {
		map.put(id, obj);
	}

	/**
	 * 移除指定的数据
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:15:40
	 * @param id
	 */
	public static void unregister(String id) {
		map.remove(id);
	}

	public Registry() {
	}
}
