package com.superman.mvpframe.client.utils;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.text.client.NumberFormatRenderer;

/**
 * 字符串工具类
 * 
 * @author superman
 * @version 2018年8月3日下午4:12:10
 */
public class StringUtils {
	private static final String PHONE_REG = "^1\\d{10}$";

	/**
	 * 去除.后面的00
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:13:25
	 * @param num
	 * @return
	 */
	public static String deleteZeroAfterPoint(double num) {
		int numInt = (int) num;
		return numInt == num ? String.valueOf(numInt) : String.valueOf(num);
	}

	/**
	 * 价格格式化0.00
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:13:39
	 * @param price
	 * @return
	 */
	public static String formatPriceStr(double price) {
		NumberFormatRenderer nf = new NumberFormatRenderer(NumberFormat.getFormat("#0.00"));
		return nf.render(price);
	}

	/**
	 * 隐藏字符串中间几个字符成*
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:13:49
	 * @param str
	 * @param startHideCount
	 * @param endHideCount
	 * @return
	 */
	public static String hideStrByStar(String str, int startHideCount, int endHideCount) {
		int length = str.length();
		if (length < 3)
			return str.substring(0, 1) + "*";
		else {
			int rest = length - startHideCount - endHideCount;
			String s1 = "(\\d{" + startHideCount + "})\\d{" + rest + "}(\\w{" + endHideCount + "})";
			String s2 = "$1";
			for (int i = 0; i < rest; i++) {
				s2 += "*";
			}
			s2 += "$2";
			return str.replaceAll(s1, s2);
		}
	}

	/**
	 * 非空校验
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:14:09
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(String string) {
		if (string == null || string.length() == 0 || string.equals("null") || string.equals("undefined")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 手机号码校验
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:14:12
	 * @param text
	 * @return
	 */
	public static boolean isTelValid(String text) {
		return text.matches(PHONE_REG);
	}

}
