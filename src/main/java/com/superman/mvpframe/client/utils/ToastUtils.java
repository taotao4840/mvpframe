package com.superman.mvpframe.client.utils;

/**
 * Toast工具类
 * 
 * @author superman
 * @version 2018年7月12日下午4:46:28
 */
public class ToastUtils {

	private static final int DURATION = 2000;

	/**
	 * 显示toast提示
	 * 
	 * @author superman
	 * @version 2018年7月12日 下午4:46:30
	 * @param msg
	 */
	public static void show(String msg) {
		toast0(msg, DURATION);
	}

	private static native void toast0(String msg, int duration)/*-{
																
																if ($doc.getElementById("toast") == undefined) {
																var m = $doc.createElement('div');
																m.id = "toast";
																m.innerHTML = msg;
																m.style.cssText = "padding:0rem 0.4rem;opacity: 0.7;min-height: 0.8rem;color: rgb(255, 255, 255);line-height: 0.8rem;text-align: center;border-radius: 0.133333rem;position: fixed;bottom: 10%;left: 50%;-webkit-transform:translateX(-50%);z-index: 999999;background: rgba(0, 0, 0,0.9);font-size: 12px;";
																$doc.body.appendChild(m);
																setTimeout(function() {
																var d = 0.5;
																m.style.webkitTransition = '-webkit-transform ' + d
																+ 's ease-in, opacity ' + d + 's ease-in';
																m.style.opacity = '0';
																setTimeout(function() {
																$doc.body.removeChild(m)
																}, d * 1000);
																}, duration);
																}
																}-*/;

}
