package com.superman.mvpframe.client.widget.citypicker;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.StyleInjector;

/**
 * 城市三联动选择器
 * 
 * @author superman
 * @version 2018年7月16日下午4:05:40
 */
public class CityPicker {

	/**
	 * 选中回调接口
	 * 
	 * @author superman
	 * @version 2018年7月16日下午4:28:48
	 */
	public interface CityPickerSelectCallback {
		/**
		 * 选中
		 * 
		 * @author superman
		 * @version 2018年7月16日 下午4:28:50
		 * @param pName 省名称,如浙江省
		 * @param cName 市名称,如杭州市
		 * @param aName 区名称,如下城区
		 * @param pCode 省编码,如330000
		 * @param cCode 市编码,如330100
		 * @param aCode 区编码,如330103
		 */
		void onSelect(String pName, String cName, String aName, String pCode, String cCode, String aCode);
	}

	private static CityPicker INSTANCE = null;

	/**
	 * 单例模式,避免外部js资源重复倒入
	 * 
	 * @author superman
	 * @version 2018年7月16日 下午4:25:06
	 * @return CityPicker
	 */
	public static CityPicker getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CityPicker();
			injectResource();
		}
		return INSTANCE;
	}

	private static void injectResource() {
		ScriptInjector.fromString(CityPickerResource.INSTANCE.muijs().getText()).setWindow(ScriptInjector.TOP_WINDOW).inject();
		ScriptInjector.fromString(CityPickerResource.INSTANCE.pickerjs().getText()).setWindow(ScriptInjector.TOP_WINDOW).inject();
		ScriptInjector.fromString(CityPickerResource.INSTANCE.poppickerjs().getText()).setWindow(ScriptInjector.TOP_WINDOW).inject();
		ScriptInjector.fromString(CityPickerResource.INSTANCE.citydata3js().getText()).setWindow(ScriptInjector.TOP_WINDOW).inject();

		StyleInjector.injectStylesheet(CityPickerResource.INSTANCE.pickercss().getText());
		StyleInjector.injectStylesheet(CityPickerResource.INSTANCE.poppickercss().getText());
	}

	/**
	 * 弹出选择器
	 * 
	 * @author superman
	 * @version 2018年7月16日 下午4:24:06
	 * @param callback
	 */
	public void show(CityPickerSelectCallback callback) {
		show0(callback);
	}

	private native void show0(CityPickerSelectCallback callback)/*-{
																
																var _getParam = function(obj, param) {
																return obj[param] || '';
																};
																
																var that = this;
																
																if(this.cityPicker3==undefined){
																	var cityPicker3 = new $wnd.mui.PopPicker({
																		layer : 3
																	});
																
																	that.cityPicker3 = cityPicker3;
																}
																
																
																this.cityPicker3.setData($wnd.cityData3);
																this.cityPicker3.show(function(items) {
																callback.@com.superman.mvpframe.client.widget.citypicker.CityPicker.CityPickerSelectCallback::onSelect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)(_getParam(items[0], 'text'),_getParam(items[1], 'text'),_getParam(items[2], 'text'),_getParam(items[0], 'value'),_getParam(items[1], 'value'),_getParam(items[2], 'value'));
																});
																}-*/;

}
