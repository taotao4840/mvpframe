package com.superman.mvpframe.client.widget.citypicker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.resources.client.TextResource;

/**
 * 外部资源倒入
 * 
 * @author superman
 * @version 2018年7月16日下午4:08:49
 */
public interface CityPickerResource extends ClientBundle {

	public CityPickerResource INSTANCE = GWT.create(CityPickerResource.class);

	@Source("city.data-3.js")
	TextResource citydata3js();

	@Source("mui.min.js")
	TextResource muijs();

	@NotStrict
	@Source("mui.picker.min.css")
	CssResource pickercss();

	@Source("mui.picker.min.js")
	TextResource pickerjs();

	@NotStrict
	@Source("mui.poppicker.css")
	CssResource poppickercss();

	@Source("mui.poppicker.js")
	TextResource poppickerjs();

}