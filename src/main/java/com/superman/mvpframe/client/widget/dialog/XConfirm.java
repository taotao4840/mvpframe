package com.superman.mvpframe.client.widget.dialog;

/**
 * 确认对话框
 * 
 * @author superman
 * @version 2018年8月12日下午9:06:03
 */
public class XConfirm {

	/**
	 * @author superman
	 * @version 2018年8月12日下午9:05:02
	 */
	public interface ConfirmCallBack {
		/**
		 * 取消
		 * 
		 * @author superman
		 * @version 2018年8月12日 下午9:05:35
		 */
		void cancel();

		/**
		 * 确认
		 * 
		 * @author superman
		 * @version 2018年8月12日 下午9:05:29
		 */
		void ok();
	}

	/**
	 * 确认对话框
	 * 
	 * @author superman
	 * @version 2018年8月12日 下午9:05:46
	 * @param msg
	 * @param callBack
	 */
	public static void confirm(String msg, ConfirmCallBack callBack) {
		confirm0(msg, callBack);
	}

	private static native void confirm0(String msg, ConfirmCallBack callBack)/*-{
																				$wnd
																				.confirm(
																				msg,
																				function(isok) {
																				if (isok) {
																				callBack.@com.superman.mvpframe.client.widget.dialog.XConfirm.ConfirmCallBack::ok()();
																				} else {
																				}
																				callBack.@com.superman.mvpframe.client.widget.dialog.XConfirm.ConfirmCallBack::cancel()();
																				
																				})
																				}-*/;

}
