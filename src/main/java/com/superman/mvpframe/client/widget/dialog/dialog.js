var dialog = {


    //对话框
    alert: function (elem, callback) {
        var content = elem.content || elem || "",
            btnText = elem.btnText || "确定",
            boxClass = elem.boxClass || "",
            alertHtml = '\
                <div class="dialog '+ boxClass +'">\
                    <div class="dialog-box">\
                        <div class="dialog-detail">' + content + '</div>\
                        <div class="dialog-opera">\
                            <button class="dialog-btn dialog-btn-close">' + btnText +  '</button>\
                        </div>\
                    </div>\
                    <div class="dialog-overlay"></div>\
                </div>';
        document.body.insertAdjacentHTML("beforeend", alertHtml);
        var dialog = document.querySelector(".dialog"),
            btnClose = dialog.querySelector(".dialog-btn-close");
        btnClose.onclick = function () {
            dialog.remove();
            if (callback) {
                callback();
            }
        };
    },
    confirm: function (elem, callback) {
        var content = elem.content || elem || "",
            okText = elem.okText || "确定",
            cancelText = elem.cancelText || "取消",
            boxClass = elem.boxClass || "",
            confirmHtml = '\
                <div class="dialog '+ boxClass +'">\
                    <div class="dialog-box">\
                        <div class="dialog-detail">' + content + '</div>\
                        <div class="dialog-opera">\
                            <button class="dialog-btn dialog-btn-cancel">' + cancelText + '</button>\
                            <button class="dialog-btn dialog-btn-ok">' + okText + '</button>\
                        </div>\
                    </div>\
                    <div class="dialog-overlay"></div>\
                </div>';
        document.body.insertAdjacentHTML("beforeend", confirmHtml);
        var dialog = document.querySelector(".dialog"),
            btnOk = dialog.querySelector(".dialog-btn-ok"),
            btnCancel = dialog.querySelector(".dialog-btn-cancel"),
            flag = true,
            result = function () {
                dialog.remove();
                if (callback) {
                    callback(flag);
                }
            };
        btnOk.onclick = function () {
            flag = true;
            result();
        };
        btnCancel.onclick = function () {
            flag = false;
            result();
        };
    }


};


//替换系统默认对话框
window.alert = dialog.alert;
window.confirm = dialog.confirm;