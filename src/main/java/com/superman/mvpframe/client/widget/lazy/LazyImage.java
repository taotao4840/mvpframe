package com.superman.mvpframe.client.widget.lazy;

import com.google.gwt.user.client.ui.Image;

/**
 * 懒加载图片
 * 
 * @author superman
 * @version 2018年7月22日上午12:11:57
 */
public class LazyImage extends Image {

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月22日 上午12:13:09
	 */
	public LazyImage() {
		super.setUrl(
				"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAAW1JREFUeAHt01ENACAMxFDAv9ZZgAQV/XhT0LS7PTN3uYyBkyEB8g0IEnsEQQSJGYjhWIggMQMxHAsRJGYghmMhgsQMxHAsRJCYgRiOhQgSMxDDsRBBYgZiOBYiSMxADMdCBIkZiOFYiCAxAzEcCxEkZiCGYyGCxAzEcCxEkJiBGI6FCBIzEMOxEEFiBmI4FiJIzEAMx0IEiRmI4ViIIDEDMRwLESRmIIZjIYLEDMRwLESQmIEYjoUIEjMQw7EQQWIGYjgWIkjMQAzHQgSJGYjhWIggMQMxHAsRJGYghmMhgsQMxHAsRJCYgRiOhQgSMxDDsRBBYgZiOBYiSMxADMdCBIkZiOFYiCAxAzEcCxEkZiCGYyGCxAzEcCxEkJiBGI6FCBIzEMOxEEFiBmI4FiJIzEAMx0IEiRmI4ViIIDEDMRwLESRmIIZjIYLEDMRwLESQmIEYjoUIEjMQw7EQQWIGYjgWIkjMQAzHQmJBHtI6BJGCdmbpAAAAAElFTkSuQmCC");
	}

	@Override
	public void setUrl(String url) {
		getElement().setAttribute("data-src", url);
	}
}
