package com.superman.mvpframe.client.widget.mescroll;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.Composite;

/**
 * 基于mescroll.js v1.3.2版本封装的控件
 * 
 * @author superman
 * @version 2018年7月15日下午3:54:17
 */
public class MeScroll extends Composite {

	protected String bottom = "0px";// 默认的绝对定位bottom
	protected UpAndDownCallBack callBack;// 数据回调接口

	protected String clearEmptyId;

	protected String meScrollId;
	protected String top = "0px";// 默认的绝对定位top.

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月15日 下午3:54:51
	 */
	public MeScroll() {
	}

	/**
	 * 结束此次的分页计算.
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午10:55:33
	 * @param listLength 当前页的数据总数
	 * @param total 数据总数
	 */
	public void endBySize(int listLength, int total) {
		endBySize0(listLength, total);
	}

	private native void endBySize0(int listLength, int total)/*-{
																this.mescroll.endBySize(listLength, total);
																}-*/;

	protected void getListData(int page) {
		if (this.callBack != null)
			callBack.getListDataByPage(page);
	}

	/**
	 * 初始化
	 * 
	 * @author superman
	 * @version 2018年7月15日 下午4:20:33
	 */
	public void init() {
		setMeScrollStyle();
		init0(meScrollId, clearEmptyId);
	}

	// /**
	// * 获取滚动条的位置y
	// *
	// * @author superman
	// * @version 2018年7月16日 下午1:56:26
	// * @return y
	// */
	// public double getScrollTop() {
	// return getScrollTop0();
	// }
	//
	// private native double getScrollTop0()/*-{
	// return this.mescroll.getScrollTop();
	// }-*/;

	private native void init0(String meScrollId, String clearEmptyId)/*-{
																		var that = this;
																		var f = function(page) {
																		that.@com.superman.mvpframe.client.widget.mescroll.MeScroll::getListData(I)(page.num);
																		}
																		var mescroll = new $wnd.MeScroll(meScrollId, {
																		up : {
																		callback : $entry(f), //上拉回调,此处可简写; 相当于 callback: function (page) { getListData(page); }
																		isBounce : false, //此处禁止ios回弹,解析(务必认真阅读,特别是最后一点): http://www.mescroll.com/qa.html#q10
																		clearEmptyId : clearEmptyId
																		//1.下拉刷新时会自动先清空此列表,再加入数据; 2.无任何数据时会在此列表自动提示空
																		
																		}
																		});
																		
																		that.mescroll = mescroll;
																		}-*/;

	/**
	 * 注入配置
	 * 
	 * @author superman
	 * @version 2018年8月22日 上午10:01:46
	 * @param meScrollId
	 * @param clearEmptyId
	 * @param top
	 * @param bottom
	 */
	public void injectConfig(String meScrollId, String clearEmptyId, String top, String bottom) {
		this.meScrollId = meScrollId;
		this.clearEmptyId = clearEmptyId;
		this.top = top;
		this.bottom = bottom;
	}

	/**
	 * /重置列表数据
	 * 
	 * @author superman
	 * @version 2018年7月16日 下午2:18:44
	 */
	public void resetUpScroll() {
		resetUpScroll0();
	}

	private native void resetUpScroll0()/*-{
										this.mescroll.resetUpScroll();
										}-*/;

	// /**
	// * 滚动列表到指定位置 ( y=0回到列表顶部; 如需滚动到列表底部,可设置y很大的值,比如y=99999 ); t时长,单位ms,默认300
	// *
	// * @author superman
	// * @version 2018年7月16日 下午1:58:15
	// * @param y
	// */
	// public void scrollTo(double y) {
	// scrollTo0(y, 0);
	// }
	//
	// private native void scrollTo0(double y, int time)/*-{
	// this.mescroll.scrollTo(y, time);
	// }-*/;

	/**
	 * 设置上拉下拉数据回调接口
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:34:25
	 * @param callBack
	 */
	public void setCallBack(UpAndDownCallBack callBack) {
		this.callBack = callBack;
	}

	// /**
	// * 设置默认的mescroll容器的布局.默认top=0,bottom=0
	// *
	// * @author superman
	// * @version 2018年7月16日 上午10:40:45
	// */
	// public void setDefaultStyle() {
	// setDefaultStyle("0px", "0px");
	// }
	//
	// /**
	// * 设置默认的mescroll容器的布局
	// *
	// * @author superman
	// * @version 2018年7月16日 上午10:40:48
	// * @param top
	// * @param bottom
	// */
	// public void setDefaultStyle(String top, String bottom) {
	// this.top = top;
	// this.bottom = bottom;
	// }

	private void setMeScrollStyle() {
		Document.get().getElementById(meScrollId).addClassName("mescroll");

		Document.get().getElementById(meScrollId).getStyle().setProperty("position", "fixed");
		Document.get().getElementById(meScrollId).getStyle().setProperty("top", top);
		Document.get().getElementById(meScrollId).getStyle().setProperty("bottom", bottom);
		Document.get().getElementById(meScrollId).getStyle().setProperty("overflowX", "hidden");
	}

}
