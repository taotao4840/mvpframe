package com.superman.mvpframe.client.widget.mescroll;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ScrollEvent;
import com.google.gwt.user.client.Window.ScrollHandler;
import com.google.gwt.user.client.ui.HTML;
import com.superman.mvpframe.client.utils.DomUtils;

/**
 * 仅仅有上拉加载的scroll控件
 * 
 * @author superman
 * @version 2018年8月21日下午3:11:28
 */
public class PullUpScroll extends MeScroll {

	private HTML bottomHTML;
	private boolean hasNextPage = false;
	private HTML noDataHTML;
	private final int PAGE_CAPACITY = 10;// 分页大小
	private int pageIndex = 1;

	private void addNoDataTips() {
		if (noDataHTML == null) {
			noDataHTML = new HTML();
			noDataHTML.setHTML("--&nbsp;没有相关数据&nbsp;--");
			noDataHTML.getElement().getStyle().setProperty("width", "100%");
			noDataHTML.getElement().getStyle().setProperty("textAlign", "center");
			noDataHTML.getElement().getStyle().setProperty("color", "#999");
			noDataHTML.getElement().getStyle().setProperty("lineHeight", "1.2rem");
			noDataHTML.getElement().getStyle().setProperty("height", "1.2rem");
			noDataHTML.getElement().getStyle().setProperty("marginTop", "1.4rem");
		}
		Document.get().getElementById(clearEmptyId).appendChild(noDataHTML.getElement());
	}

	private void clear() {
		Document.get().getElementById(clearEmptyId).removeAllChildren();
	}

	@Override
	public void endBySize(int listLength, int total) {
		// 无数据
		if (total == 0) {
			addNoDataTips();
			bottomHTML.setVisible(false);
		} else {
			if (listLength < PAGE_CAPACITY) {
				hasNextPage = false;
				if (pageIndex == 1) {
					// 只有一页.隐藏底部的文字
					bottomHTML.setVisible(false);
				} else {
					// 当前也是最后一页,显示最后一页的文字
					bottomHTML.setVisible(true);
					bottomHTML.setHTML("--&nbsp;我是有底线的&nbsp;--");
				}
			} else {
				// 有下一页,显示正在加载的文字
				hasNextPage = true;
				bottomHTML.setVisible(true);
				bottomHTML.setText("正在加载...");
			}
		}
	}

	private void getList() {
		bottomHTML.setText("正在加载...");
		bottomHTML.setVisible(true);
		hasNextPage = false;
		callBack.getListDataByPage(pageIndex);
	}

	@Override
	public void init() {
		Document.get().getElementById(meScrollId).getStyle().setProperty("marginBottom", bottom);

		bottomHTML = new HTML();
		bottomHTML.setVisible(false);
		bottomHTML.getElement().getStyle().setProperty("width", "100%");
		bottomHTML.getElement().getStyle().setProperty("textAlign", "center");
		bottomHTML.getElement().getStyle().setProperty("color", "#999");
		bottomHTML.getElement().getStyle().setProperty("lineHeight", "1.2rem");
		bottomHTML.getElement().getStyle().setProperty("height", "1.2rem");
		Document.get().getElementById(meScrollId).appendChild(bottomHTML.getElement());

		getList();

		Window.addWindowScrollHandler(new ScrollHandler() {

			@Override
			public void onWindowScroll(ScrollEvent event) {
				if (DomUtils.isScrollToBottom() && hasNextPage) {
					pageIndex++;
					getList();
				}
			}
		});
	}

	@Override
	public void resetUpScroll() {
		pageIndex = 1;
		clear();
		getList();
	}

}
