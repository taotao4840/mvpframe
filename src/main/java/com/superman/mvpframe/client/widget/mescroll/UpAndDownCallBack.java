package com.superman.mvpframe.client.widget.mescroll;

/**
 * 上拉和下拉数据回调接口
 * 
 * @author superman
 * @version 2018年7月16日上午11:20:21
 */
public interface UpAndDownCallBack {

	/**
	 * 根据页码获取这一页的数据
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:20:23
	 * @param page
	 */
	void getListDataByPage(int page);

}
