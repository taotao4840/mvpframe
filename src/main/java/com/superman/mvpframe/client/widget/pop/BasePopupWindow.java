package com.superman.mvpframe.client.widget.pop;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Overflow;

/**
 * 基础弹窗，弹出时，底下的页面不可滑动
 * 
 * @author superman
 * @version 2018年7月24日下午11:59:17
 */
public class BasePopupWindow extends FadePopupWindow {

	public BasePopupWindow() {
		super();
	}

	@Override
	public void hide() {
		super.hide();

		Document.get().getBody().getParentElement().getStyle().clearOverflow();

		try {
			int top = -Integer.valueOf(Document.get().getBody().getStyle().getTop().replace("px", ""));
			Document.get().getBody().getStyle().clearTop();
			Document.get().getBody().getStyle().clearBottom();
			Document.get().getBody().getStyle().clearPosition();
			// if (top != 0) {
			Document.get().getBody().setScrollTop(top);
			// }
		} catch (Exception e) {

		}
	}

	@Override
	public void show() {
		super.show();

		Document.get().getBody().getParentElement().getStyle().setOverflow(Overflow.HIDDEN);
		// 将BasePopupWindow的scrollTop为0的判断去掉,否则当页面的scrollTop为0时,弹窗出现时页面仍然可以滑动
		// if (Document.get().getBody().getScrollTop() != 0) {
		Document.get().getBody().getStyle().setProperty("top", -Document.get().getBody().getScrollTop() + "px");
		Document.get().getBody().getStyle().setProperty("bottom", "0");
		Document.get().getBody().getStyle().setProperty("position", "fixed");
		// }
	}
}
