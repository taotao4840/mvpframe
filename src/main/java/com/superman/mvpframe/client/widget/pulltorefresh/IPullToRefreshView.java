package com.superman.mvpframe.client.widget.pulltorefresh;

import java.util.List;
import java.util.Map;

import com.superman.mvpframe.client.widget.mescroll.UpAndDownCallBack;

/**
 * 下拉刷新View层接口
 * 
 * @author superman
 * @version 2018年7月16日上午11:32:00
 */
public interface IPullToRefreshView {

	/**
	 * 重置列表为第一页 (常用于列表筛选条件变化或切换菜单时重新刷新列表数据)
	 * 
	 * @author superman
	 * @version 2018年7月16日 下午2:20:30
	 */
	void clearListAndResetScroll();

	/**
	 * 结束本次分页,置入数据,ui粘合
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:32:15
	 * @param list
	 * @param total
	 */
	void endBySize(List<Map<String, Object>> list, int total);

	/**
	 * 设置上拉和下拉的数据回调接口,并初始化
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:32:17
	 * @param callback
	 */
	void initMeScroll(UpAndDownCallBack callback);

	/**
	 * 设置上拉和下拉的数据回调接口,并初始化
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:32:17
	 * @param callback
	 * @param top
	 * @param bottom
	 */
	void initMeScroll(UpAndDownCallBack callback, String top, String bottom);

	// /**
	// * 滚到上一次滚动条的位置.用于返回时的操作
	// *
	// * @author superman
	// * @version 2018年7月16日 下午2:06:44
	// */
	// void scrollToLastPosition();
	//
	// /**
	// * 滚动顶部
	// *
	// * @author superman
	// * @version 2018年7月16日 下午2:06:41
	// */
	// void scrollToTop();

}
