package com.superman.mvpframe.client.widget.pulltorefresh;

import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Composite;
import com.superman.mvpframe.client.widget.mescroll.MeScroll;
import com.superman.mvpframe.client.widget.mescroll.UpAndDownCallBack;

/**
 * 上拉下拉ui层虚类方法
 * 
 * @author superman
 * @version 2018年7月16日上午11:23:13
 */
public abstract class PullToRefreshViewAbstractImpl extends Composite implements IPullToRefreshView {

	private final String CLEAR_EMPTY_ID = "dataList_" + System.currentTimeMillis();
	private MeScroll meScroll;
	private final String MESCROLL_ID = "mescroll_" + System.currentTimeMillis();

	// private double top = 0;

	@Override
	public void clearListAndResetScroll() {
		meScroll.resetUpScroll();
	}

	@Override
	public void endBySize(List<Map<String, Object>> list, int total) {
		meScroll.endBySize(list == null ? 0 : list.size(), total);
		if (list != null && list.size() > 0)
			setListData(list);
	}

	@Override
	public void initMeScroll(UpAndDownCallBack callback) {
		initMeScroll(callback, "0px", "0px");

	}

	@Override
	public void initMeScroll(UpAndDownCallBack callback, String top, String bottom) {
		if (meScroll == null) {
			setMeScrollDivId(MESCROLL_ID);
			setDataListDivId(CLEAR_EMPTY_ID);
			// 自己实现的带上拉加载更多的组件
			meScroll = GWT.create(MeScroll.class);
			meScroll.injectConfig(MESCROLL_ID, CLEAR_EMPTY_ID, top, bottom);
			// 基于mescroll.js封装的带上拉和下拉的组件
			// meScroll = new MeScroll(MESCROLL_ID, CLEAR_EMPTY_ID, top, bottom);
			meScroll.setCallBack(callback);
			meScroll.init();
		}

		// scrollToTop();
	}

	// @Override
	// protected void onDetach() {
	// super.onDetach();
	// // 离开时记录当前的top
	// // this.top = meScroll.getScrollTop();
	// }

	// @Override
	// public void scrollToLastPosition() {
	// // meScroll.scrollTo(top);
	// }
	//
	// @Override
	// public void scrollToTop() {
	// this.top = 0;
	// meScroll.scrollTo(top);
	// }

	/**
	 * 设置数据层的div元素id
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:33:29
	 * @param id
	 */
	public abstract void setDataListDivId(String id);

	/**
	 * 将本页的数据粘合到容器中
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:34:00
	 * @param list
	 */
	public abstract void setListData(List<Map<String, Object>> list);

	/**
	 * 设置mescroll层的div元素id
	 * 
	 * @author superman
	 * @version 2018年7月16日 上午11:33:27
	 * @param id
	 */
	public abstract void setMeScrollDivId(String id);

}
