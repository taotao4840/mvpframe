package com.superman.mvpframe.client.widget.spinner;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

/**
 * CSS3动画,钟
 * 
 * @author superman
 * @version 2017年5月25日下午5:33:29
 */
public class TimerSpinner extends Widget {

	interface TimerSpinnerUiBinder extends UiBinder<Element, TimerSpinner> {
	}

	private static TimerSpinnerUiBinder uiBinder = GWT.create(TimerSpinnerUiBinder.class);

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2017年5月25日 下午5:33:30
	 */
	public TimerSpinner() {
		setElement(uiBinder.createAndBindUi(this));
	}

}
