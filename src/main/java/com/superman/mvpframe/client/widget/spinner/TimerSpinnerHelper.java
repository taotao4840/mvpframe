package com.superman.mvpframe.client.widget.spinner;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * 进度条加载,单例模式
 * 
 * @author superman
 * @version 2018年8月13日上午1:39:14
 */
public class TimerSpinnerHelper {
	private static TimerSpinnerHelper INSTANCE = null;

	/**
	 * 返回实力
	 * 
	 * @author superman
	 * @version 2018年8月13日 上午1:39:23
	 * @return TimerSpinnerHelper
	 */
	public static TimerSpinnerHelper getInstance() {
		if (INSTANCE == null)
			INSTANCE = new TimerSpinnerHelper();
		return INSTANCE;
	}

	private boolean show = false;
	private TimerSpinner spinner;

	private FlowPanel wrapper;

	/**
	 * 隐藏
	 * 
	 * @author superman
	 * @version 2018年8月13日 上午1:39:35
	 */
	public void hide() {
		if (wrapper != null && show) {
			show = false;
			wrapper.removeFromParent();
		}
	}

	/**
	 * 展示
	 * 
	 * @author superman
	 * @version 2018年8月13日 上午1:39:31
	 */
	public void show() {
		if (!show) {
			show = true;
			if (wrapper == null) {
				wrapper = new FlowPanel();
				wrapper.addStyleName("flex-h flex-hc flex-vc");
				wrapper.getElement().getStyle().setProperty("position", "absolute");
				wrapper.getElement().getStyle().setProperty("top", "0");
				wrapper.getElement().getStyle().setProperty("bottom", "0");
				wrapper.getElement().getStyle().setProperty("left", "0");
				wrapper.getElement().getStyle().setProperty("right", "0");
				spinner = new TimerSpinner();
				wrapper.add(spinner);
			}
			RootPanel.get().add(wrapper);
		}
	}
}
