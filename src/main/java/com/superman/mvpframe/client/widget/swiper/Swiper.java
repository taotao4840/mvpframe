package com.superman.mvpframe.client.widget.swiper;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * 基于swiper4.x封装的轮播控件.默认已封装自动轮播无限循环
 * 
 * @author superman
 * @version 2018年8月21日上午10:23:49
 */
public class Swiper extends FlowPanel {

	private FlowPanel wrapperPanel;

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年8月21日 上午10:24:20
	 */
	public Swiper() {
		this.getElement().addClassName("swiper-container");
		this.setSize("100%", "auto");

		wrapperPanel = new FlowPanel();
		wrapperPanel.getElement().addClassName("swiper-wrapper");

		FlowPanel paginationPanel = new FlowPanel();
		paginationPanel.getElement().addClassName("swiper-pagination");

		this.add(wrapperPanel);
		this.add(paginationPanel);
	}

	/**
	 * 添加
	 * 
	 * @author superman
	 * @version 2018年8月21日 上午10:29:03
	 * @param widget
	 */
	public void addItem(Widget widget) {
		if (widget != null) {
			widget.getElement().addClassName("swiper-slide");
			wrapperPanel.add(widget);
		}
	}

	@Override
	public void clear() {
		wrapperPanel.clear();
	}

	/**
	 * 初始化
	 * 
	 * @author superman
	 * @version 2018年8月21日 上午10:30:14
	 */
	public void init() {
		init0();
	}

	private native void init0()/*-{
								$wnd.initSwiper();
								}-*/;

}
