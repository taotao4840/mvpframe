package com.superman.mvpframe.client.widget.tabbar;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;
import com.superman.mvpframe.client.widget.tabbar.MyTabBarSelectEvent.MyTabBarSelectHandler;

/**
 * 选中事件处理
 * 
 * @author superman
 * @version 2018年8月7日下午1:37:50
 */
public interface HasMyTabBarSelectHandler extends HasHandlers {
	/**
	 * 选中
	 * 
	 * @author superman
	 * @version 2018年8月7日 下午1:38:10
	 * @param myTabBarSelectHandler
	 * @return HandlerRegistration
	 */
	public HandlerRegistration addMyTabBarSelectHandler(MyTabBarSelectHandler myTabBarSelectHandler);
}
