package com.superman.mvpframe.client.widget.tabbar;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.superman.mvpframe.client.widget.tabbar.MyTabBarSelectEvent.MyTabBarSelectHandler;

/**
 * 选中事件,内部调用
 * 
 * @author superman
 * @version 2018年8月7日下午1:39:10
 */
public class MyTabBarSelectEvent extends GwtEvent<MyTabBarSelectHandler> {

	public interface MyTabBarSelectHandler extends EventHandler {
		public void onTabBarSelected(MyTabBarSelectEvent event);
	}

	private static final GwtEvent.Type<MyTabBarSelectHandler> TYPE = new GwtEvent.Type<MyTabBarSelectHandler>();

	public static void fire(HasMyTabBarSelectHandler source, int index) {
		if (TYPE != null) {
			MyTabBarSelectEvent event = new MyTabBarSelectEvent(index);
			source.fireEvent(event);
		}
	}

	public static GwtEvent.Type<MyTabBarSelectHandler> getType() {
		return TYPE;
	}

	private int index;

	public MyTabBarSelectEvent(int index) {
		super();
		this.index = index;
	}

	@Override
	protected void dispatch(MyTabBarSelectHandler handler) {
		handler.onTabBarSelected(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MyTabBarSelectHandler> getAssociatedType() {
		return TYPE;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}
