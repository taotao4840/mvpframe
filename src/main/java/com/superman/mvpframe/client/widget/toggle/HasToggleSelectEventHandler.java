package com.superman.mvpframe.client.widget.toggle;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;
import com.superman.mvpframe.client.widget.toggle.ToggleSelectEvent.ToggleSelectEventHandler;

/**
 * 内部调用.选中处理
 * 
 * @author superman
 * @version 2018年8月3日下午4:06:53
 */
public interface HasToggleSelectEventHandler extends HasHandlers {
	/**
	 * @author superman
	 * @version 2018年8月3日 下午4:06:55
	 * @param handler
	 * @return
	 */
	public HandlerRegistration addToggleSelectEventHandler(ToggleSelectEventHandler handler);
}
