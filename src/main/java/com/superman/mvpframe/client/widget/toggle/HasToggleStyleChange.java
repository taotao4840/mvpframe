package com.superman.mvpframe.client.widget.toggle;

/**
 * 字元素切换接口
 * 
 * @author superman
 * @version 2018年8月3日下午4:07:31
 */
public interface HasToggleStyleChange {

	/**
	 * 切换选中样式
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:07:35
	 */
	void changeStyle();

	/**
	 * 重制样式
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:07:33
	 */
	void resetDefaultStyle();

}
