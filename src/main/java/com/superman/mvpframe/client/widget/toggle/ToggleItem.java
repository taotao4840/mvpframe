package com.superman.mvpframe.client.widget.toggle;

import com.google.gwt.user.client.ui.Composite;

/**
 * 切换的子元素
 * 
 * @author superman
 * @version 2018年8月3日下午4:08:27
 */
public abstract class ToggleItem extends Composite implements HasToggleStyleChange {

	private Object obj;// 上下文对象

	/**
	 * @author superman
	 * @version 2018年8月3日 下午4:08:29
	 * @return
	 */
	public Object getObj() {
		return obj;
	}

	/**
	 * @author superman
	 * @version 2018年8月3日 下午4:08:31
	 * @param obj
	 */
	public void setObj(Object obj) {
		this.obj = obj;
	}

}
