package com.superman.mvpframe.client.widget.toggle;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.superman.mvpframe.client.widget.toggle.ToggleSelectEvent.ToggleSelectEventHandler;

/**
 * 切换控件
 * 
 * @author superman
 * @version 2018年8月3日下午4:08:58
 */
public class ToggleList extends Composite implements HasToggleSelectEventHandler {

	private ToggleItem item;
	protected List<ToggleItem> items = new ArrayList<ToggleItem>();

	/**
	 * 添加子元素
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:03
	 * @param item
	 */
	public void addItemToDict(final ToggleItem item) {
		if (items == null)
			items = new ArrayList<ToggleItem>();
		items.add(item);

		item.addDomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				select(item);
			}
		}, ClickEvent.getType());
	}

	@Override
	public HandlerRegistration addToggleSelectEventHandler(ToggleSelectEventHandler handler) {
		return addHandler(handler, ToggleSelectEvent.getType());
	}

	private void changeStyle(ToggleItem item) {
		for (ToggleItem toggleItem : items) {
			toggleItem.resetDefaultStyle();
			if (item.equals(toggleItem))
				item.changeStyle();
		}
	}

	/**
	 * 清空
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:00
	 */
	public void clear() {
		items.clear();
	}

	/**
	 * 选中并触发选中事件
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:11
	 * @param item
	 */
	public void select(ToggleItem item) {
		select(item, true);
	}

	/**
	 * 选中
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:13
	 * @param item
	 * @param active
	 */
	public void select(ToggleItem item, boolean active) {
		this.item = item;
		changeStyle(item);
		if (active)
			ToggleSelectEvent.fire(this, item, items.indexOf(item));
	}

	/**
	 * 选中并触发选中事件
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:06
	 * @param index
	 */
	public void selectByIndex(int index) {
		selectByIndex(index, true);
	}

	/**
	 * 选中
	 * 
	 * @author superman
	 * @version 2018年8月3日 下午4:09:08
	 * @param index
	 * @param active
	 */
	public void selectByIndex(int index, boolean active) {
		select(items.get(index), active);
	}

}
