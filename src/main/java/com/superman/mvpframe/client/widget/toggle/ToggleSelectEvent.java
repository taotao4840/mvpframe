package com.superman.mvpframe.client.widget.toggle;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.superman.mvpframe.client.widget.toggle.ToggleSelectEvent.ToggleSelectEventHandler;

/**
 * 选中切换事件,内部调用
 * 
 * @author superman
 * @version 2018年7月25日下午4:02:51
 */
public class ToggleSelectEvent extends GwtEvent<ToggleSelectEventHandler> {

	/**
	 * 事件处理器
	 * 
	 * @author superman
	 * @version 2018年7月25日下午4:03:11
	 */
	public interface ToggleSelectEventHandler extends EventHandler {
		/**
		 * 选中
		 * 
		 * @author superman
		 * @version 2018年7月25日 下午4:03:13
		 * @param event
		 */
		public void onSelected(ToggleSelectEvent event);
	}

	private static final GwtEvent.Type<ToggleSelectEventHandler> TYPE = new GwtEvent.Type<ToggleSelectEventHandler>();

	/**
	 * 触发
	 * 
	 * @author superman
	 * @version 2018年7月25日 下午4:03:27
	 * @param source
	 * @param toggleItem
	 * @param selectedIndex
	 */
	public static void fire(HasToggleSelectEventHandler source, ToggleItem toggleItem, int selectedIndex) {
		if (TYPE != null) {
			ToggleSelectEvent event = new ToggleSelectEvent();
			event.setToggleItem(toggleItem);
			event.setSelectedIndex(selectedIndex);
			source.fireEvent(event);
		}
	}

	public static GwtEvent.Type<ToggleSelectEventHandler> getType() {
		return TYPE;
	}

	private int selectedIndex;

	private ToggleItem toggleItem;

	@Override
	protected void dispatch(ToggleSelectEventHandler handler) {
		handler.onSelected(this);

	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ToggleSelectEventHandler> getAssociatedType() {
		return TYPE;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public ToggleItem getToggleItem() {
		return toggleItem;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public void setToggleItem(ToggleItem toggleItem) {
		this.toggleItem = toggleItem;
	}

}
