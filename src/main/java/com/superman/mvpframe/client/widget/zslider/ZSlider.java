package com.superman.mvpframe.client.widget.zslider;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * 基于zslider.js封装的轮播控件
 * 
 * @author superman
 * @version 2018年7月14日下午1:28:20
 */
public class ZSlider extends Composite {

	private FlowPanel contentPanel;

	private boolean init = false;

	private FlowPanel wrapPanel;

	/**
	 * 构造方法
	 * 
	 * @author superman
	 * @version 2018年7月14日 下午1:29:53
	 */
	public ZSlider() {
		super();

		wrapPanel = new FlowPanel();
		contentPanel = new FlowPanel();

		wrapPanel.getElement().addClassName("z-slide-wrap");
		contentPanel.getElement().addClassName("z-slide-content");

		wrapPanel.add(contentPanel);
		wrapPanel.getElement().getStyle().setOverflowX(Overflow.HIDDEN);

		this.initWidget(wrapPanel);

	}

	/**
	 * 添加轮播元素
	 * 
	 * @author superman
	 * @version 2018年7月14日 下午1:34:12
	 * @param item
	 */
	public void addItem(Widget item) {
		if (item != null && !init) {
			item.getElement().addClassName("z-slide-item");
			contentPanel.add(item);
		}

	}

	/**
	 * 清空
	 * 
	 * @author superman
	 * @version 2018年8月7日 上午12:12:10
	 */
	public void clear() {
		contentPanel.clear();
	}

	/**
	 * 初始化轮播组件,该方法需要在轮播元素添加完成后方可调用
	 * 
	 * @author superman
	 * @version 2018年7月14日 下午1:34:29
	 */
	public void init() {
		init(true);
	}

	/**
	 * 初始化轮播组件,该方法需要在轮播元素添加完成后方可调用
	 * 
	 * @author superman
	 * @version 2018年7月14日 下午1:34:29
	 * @param autyplay
	 */
	public void init(boolean autyplay) {
		if (!init && contentPanel.getWidgetCount() > 0) {
			init = true;
			init0(wrapPanel.getElement(), autyplay);
		}
	}

	private native void init0(Element wrapEle, boolean autyplay)/*-{
		var that = this;
		var slide = new $wnd.Slider(wrapEle, '.z-slide-item', {
			interval : 5,
			duration : 0.5,
			autoplay : autyplay
		});

	}-*/;

}
