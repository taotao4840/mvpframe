/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved. All rights reserved. This program and
 * the accompanying materials are made available under the terms of the Eclipse Public
 * License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Unless required by applicable law or agreed
 * to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under the
 * License.
 *******************************************************************************/
/**
 * 这里放置的是基于zslider.js封装的轮播控件. 跟业务无关
 * 
 * @author superman
 * @version 2018年7月13日下午2:41:41
 */
package com.superman.mvpframe.client.widget.zslider;