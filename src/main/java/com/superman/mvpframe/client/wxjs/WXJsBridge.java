package com.superman.mvpframe.client.wxjs;

/**
 * 微信JSBridge工具类
 * 
 * @author superman
 * @version 2019年1月17日下午10:37:03
 */
public class WXJsBridge {
	// 当前页面WX-JSSDK通道是否配置成功
	private static boolean jsBridgeReady = false;

	/**
	 * 通过config接口注入权限验证配置
	 * 
	 * @author superman
	 * @version 2019年1月17日 下午10:37:17
	 * @param debug
	 *        开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
	 * @param appId 必填，公众号的唯一标识
	 * @param timestamp 必填，生成签名的时间戳
	 * @param nonceStr 必填,生成签名的随机串
	 * @param signature 必填,签名
	 */
	public static void config(boolean debug, String appId, String timestamp, String nonceStr, String signature) {
		config0(debug, appId, timestamp, nonceStr, signature);
		injectReadyJsni();
	}

	private static native void config0(boolean debug, String appId, String timestamp, String nonceStr, String signature)/*-{
		$wnd.wx.config({
			debug : debug,
			appId : appId,
			timestamp : timestamp,
			nonceStr : nonceStr,
			signature : signature,
			jsApiList : [ 'checkJsApi', 'onMenuShareTimeline',
					'onMenuShareAppMessage', 'onMenuShareQQ',
					'onMenuShareWeibo', 'onMenuShareQZone', 'hideMenuItems',
					'showMenuItems', 'hideAllNonBaseMenuItem',
					'showAllNonBaseMenuItem', 'translateVoice', 'startRecord',
					'stopRecord', 'onVoiceRecordEnd', 'playVoice',
					'onVoicePlayEnd', 'pauseVoice', 'stopVoice', 'uploadVoice',
					'downloadVoice', 'chooseImage', 'previewImage',
					'uploadImage', 'downloadImage', 'getNetworkType',
					'openLocation', 'getLocation', 'hideOptionMenu',
					'showOptionMenu', 'closeWindow', 'scanQRCode',
					'chooseWXPay', 'openProductSpecificView', 'addCard',
					'chooseCard', 'openCard' ]
		});
	}-*/;

	private static native void injectReadyJsni()/*-{
		var that = this;
		$wnd.wx
				.ready(function() {
					that.@com.superman.mvpframe.client.wxjs.WXJsBridge::setJsBridgeReady()();
				});
		$wnd.wx.error(function(res) {
			alert(res);
		})
	}-*/;

	/**
	 * 微信JSSDK是否配置成功
	 * 
	 * @author superman
	 * @version 2019年1月17日 下午10:45:37
	 * @return true or false
	 */
	public static boolean isJsBridgeReady() {
		return jsBridgeReady;
	}

	private static void setJsBridgeReady() {
		WXJsBridge.jsBridgeReady = true;
	}

}
