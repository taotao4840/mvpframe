package com.superman.mvpframe.client.wxjs;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 微信支付
 * 
 * @author superman
 * @version 2019年1月17日下午10:52:15
 */
public class WXPay {

	/**
	 * 微信支付
	 * 
	 * @author superman
	 * @version 2019年1月17日 下午10:52:07
	 * @param timestamp
	 *        支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
	 * @param nonceStr 支付签名随机串，不长于 32 位
	 * @param packages 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
	 * @param signType 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
	 * @param paySign 支付签名
	 * @param callback 回调接口
	 */
	public static void chooseWXPay(String timestamp, String nonceStr, String packages, String signType, String paySign,
			AsyncCallback<Void> callback) {
		chooseWXPay0(timestamp, nonceStr, packages, signType, paySign, callback);
	}

	private static native void chooseWXPay0(String timestamp, String nonceStr, String packages, String signType, String paySign,
			AsyncCallback<Void> callback)/*-{
		$wnd.wx
				.chooseWXPay({
					timestamp : timestamp,
					nonceStr : nonceStr,
					'package' : packages,
					signType : signType,
					paySign : paySign,
					success : function(res) {
						callback.@com.google.gwt.user.client.rpc.AsyncCallback::onSuccess()();
					},
					cancel : function(res) {
						callback.@com.google.gwt.user.client.rpc.AsyncCallback::onFailure()();
					}
				})
	}-*/;

}
